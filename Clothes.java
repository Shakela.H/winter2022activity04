public class Clothes{
	private String type;
	private String size;
	private double price;
	
	public void fit(){
		if (this.size.equals("large")){
			System.out.println("This is A size large, might be too big for you.");
		}else if (this.size.equals("medium")){
			System.out.println("It's a perfect fit!");
		}else if (this.size.equals("small")){
			System.out.println("Might be a tight fit on you");
		}else{
			System.out.println("Are you sure you entered the right size?");
		}
	}
	//getters
	
	public String getType(){
		return this.type;
	}
	
	public String getSize(){
		return this.size;
	}
	
	public double getPrice(){
		return this.price;
	}
	//setters
	
	//public void setType(String newType){
	//	this.type = newType;
	//}
	
	public void setSize(String newSize){
		this.size = newSize;
	}
	
	public void setPrice(double newPrice){
		this.price = newPrice;
	}
	
	//constructor
	
	public Clothes(String type, String size, double price){
		this.type = type;
		this.size = size;
		this.price = price;
	}
	
}