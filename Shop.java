import java.util.Scanner;
public class Shop{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		Clothes[] clothing = new Clothes[4];
		for (int i = 0; i < clothing.length ; i++){// loop to store object values into the array
			
			
			System.out.println("What piece of clothing for #" + (i+1) + " are you looking for?: shirt, dress, pants, etc");
			String settingType = sc.next();
			//(DELETED)clothing[i].setType(settingType); //taking input for type of clothing 
			
			
			System.out.println("Enter the size: ");
			String settingSize = sc.next();
			//(DELETED)clothing[i].setSize(settingSize); //taking input of size
			
			System.out.println("Enter the price");
			double settingPrice = sc.nextDouble();
			//(DELETED)clothing[i].setPrice(settingPrice);
			System.out.println(""); // taking input of price
			
			clothing[i] = new Clothes(settingType,settingSize, settingPrice);// constructor taking 3 values as  parameters 
			
		}
		//receipt portion
		
		System.out.println("Please update your choice for the size:");
		String updatingSize = sc.next();
		clothing[3].setSize(updatingSize);
		
		System.out.println("Finaly, update your choice for the Price:");
		double updatingPrice = sc.nextDouble();
		clothing[3].setPrice(updatingPrice);
		
		System.out.println("Receipt of your last item:");
		System.out.println("Your item: "+clothing[3].getType());
		System.out.println("Size you have chosen: "+ clothing[3].getSize());
		System.out.println("Price point: " + clothing[3].getPrice()+"$");
		System.out.println("Notes:");
		clothing[3].fit();//calling the instance method from part 1 of the assignment 
 //note to self, add a portion for users to input their own size so we can compare with the information we have
	}
}